// HOMEPAGE FIXED MENU

$(document).on('click', 'li.nav-item a[href^="#"], .scroll-down a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top - 100;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos});
     
        
});


// CHANGE BUTTON COLOR IF HAS VALUE

function initBtnChange() {
  $('input#forgot-pass-email').on('input', function(event) {
    var inputValue = this.value;
    if (inputValue) {
      $('.btn.disabled').addClass('btn-blue').removeClass('disabled');
    } else {
      $('.btn.disabled').addClass('disabled').removeClass('btn-blue');
    }
  });
}
//init
initBtnChange();

function initBtnChange2() {
  $('.new-pass1 input, .new-pass1 input').on('input', function(event) {
    var inputValue = this.value;
    if (inputValue) {
      $('.btn.disabled').addClass('btn-blue').removeClass('disabled').prop("disabled", false);
    } else {
      $('.btn.btn-blue').addClass('disabled').removeClass('btn-blue').prop("disabled", true);
    }
  });
}
//init
initBtnChange2();


// SHOW THANK YOU MESSAGE

$('button#verify-email-button').click(function(){
  $('.non-verify').hide(200);
  $('.done-verify').show(200);

});


// VIEW PASSWORD

$(document).ready(function() {
    $(".new-pass1 img").on('click', function(event) {
        event.preventDefault();
        if($('.new-pass1 input').attr("type") == "text"){
            $('.new-pass1 input').attr('type', 'password');
        }
        else if($('.new-pass1 input').attr("type") == "password"){
            $('.new-pass1 input').attr('type', 'text');
        }
    });
    $(".new-pass2 img").on('click', function(event) {
        if($('.new-pass2 input').attr("type") == "text"){
            $('.new-pass2 input').attr('type', 'password');
        }
        else if($('.new-pass2 input').attr("type") == "password"){
            $('.new-pass2 input').attr('type', 'text');
        }
    });
});


// COUNTDOWN 
const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;

let countDown = new Date('Apr 5, 2019 00:00:00').getTime(),
    x = setInterval(function() {

      let now = new Date().getTime(),
          distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
        document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
      }, second)